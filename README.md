# Whatsapp Spam Bot

# O que é?
Basicamente, é um script que manda a mensagem que você quiser várias vezes para um de seus contatos.
## Como rodar?
Primeiramente, você precisa do Python 3.8.5 ou uma versão maior. Também será necessário a instalação do pip para instalar a biblioteca selenium com o seguinte código em seu terminal/prompt de comando: `pip install selenium`
O próximo passo é ter instalado no computador o Google Chrome, baixar o [Chromedriver](http://chromedriver.chromium.org/downloads) de acordo com a versão de seu navegador e sistema operacional, e colocá-lo na mesma pasta/diretório que o arquivo **app.py**.
Agora é só executar o comando: `python app.py`

### Obs: Execute o script com a permissão da pessoa ou grupo que você quer testar. Lembre-se que você é responsável por todas as suas ações ;)

### Autores:
- [Daniel Batistão de Paula](https://gitlab.com/danielsbp)
- [Dalton Pacola Franco](https://gitlab.com/daltonpacolafranco)
