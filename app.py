from selenium import webdriver
import os
from time import sleep

contato = input('Digite o nome do contato igual o que está no Whatsapp: ')
mensagem = input('Digite a mensagem que será enviada: ')
qtdmensagens = int(input('Quantas mensagens você deseja enviar?'))

print('https://web.whatsapp.com')
print('O Google Chrome será aberto. Você tem 25 segundos para entrar no Web Whatsapp com o QR Code')


dir_path = os.getcwd()
# O caminho do chromedriver
chromedriver = os.path.join(dir_path, "chromedriver")
# Caminho onde será criada pasta profile
profile = os.path.join(dir_path, "profile", "wpp")

options = webdriver.ChromeOptions()
# Configurando a pasta profile, para mantermos os dados da seção
options.add_argument(r"user-data-dir={}".format(profile))
# Inicializa o webdriver
driver = webdriver.Chrome(
chromedriver, chrome_options=options)
# Abre o whatsappweb
driver.get("https://web.whatsapp.com/")
# Aguarda alguns segundos para validação manual do QrCode
driver.implicitly_wait(25)


busca = driver.find_element_by_class_name('_1awRl')
busca.click()
busca.send_keys(contato)
sleep(4)
# Seleciona o contato
contato_elemento=driver.find_element_by_xpath("//span[@title = '{}']".format(contato))
contato_elemento.click()

for x in range(0, qtdmensagens):
	caixa_de_texto=driver.find_element_by_xpath("/html/body/div[1]/div/div/div[4]/div/footer/div[1]/div[2]/div/div[2]")
	caixa_de_texto.click()
	caixa_de_texto.send_keys(mensagem)
	sleep(2)
	botao_de_enviar=driver.find_element_by_class_name('_2Ujuu')
	botao_de_enviar.click()

